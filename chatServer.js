
var net = require('net');
var sys = require('sys');
var os = require('os');
var HOST = '127.0.0.1';
var PORT = 6969;
var sockets = [];  // socket array to store all connected socket.
var nameList = []; // name array to store all user name
var roomList = {}; // { 'roomname': {members: [] } }

function roomInfo() {
    this.members = [];
};

// status
var LOGIN_STATUS = 0;
var ROOM_STATUS = 1;
var CHAT_STATUS = 2;

// commands
var ROOMS_CMD = '/rooms';
var JOIN_CMD = '/join';
var CREATE_CMD = '/create';
var DELETE_CMD = '/delete';
var LEAVE_CMD = '/leave';
var QUIT_CMD = '/quit';


function getPureString(s) {
    return s.replace(/(\r\n|\n|\r)/gm,"");
}

function removeSocket(sock) {
    // remove socket
    var idx = sockets.indexOf(sock);
    if (idx != -1) {
        delete sockets[idx];
        sockets.splice(idx, 1);
    }

    // remove name
    var name = sock.username;
    idx = nameList.indexOf(name);
    if (idx != -1) {
        nameList.splice(idx, 1);
    }
}

function broadcastToAllUser(sock, data) {
    // Broad cast message to all user
    var len = sockets.length;
    for (var i = 0; i < len; i ++) {
        if (sockets[i] != sock) {
            if (sockets[i]) {
                sockets[i].write(sock.remoteAddress + ':' 
                    + sock.remotePort + ':' + data + '\n');
            }
        }
    }
}

function broadcastToRoom(sock, data) {
    // Broad cast message to this room
    var len = sockets.length;
    for (var i = 0; i < len; i ++) {
        var s = sockets[i];

        // Send to the same room member, and avoid send to himself.
        if (s != sock && s.roomname === sock.roomname ) {
            if (s) {
                s.write('' + sock.username + ' say: ' + data +'\n');
            }
        }
    }
}

function showRooms(sock) {

    var roomOptions = 'Active rooms are: \n';
    var room;

    for ( var key in roomList ) {
        room = roomList[key];
        roomOptions += ' * ' + key + '(' + room.members.length + ')\n';
    }

    sock.write(roomOptions + 'end of list.\n');    
}

function someoneJoinRoom(sock) {

    var msg = '* new user joined chat: ' + sock.username + '\n';
    var msgOwner = '* new user joined chat: ' + sock.username
                    + ' (** this is you) \n';

    // Broad cast message to this room
    var len = sockets.length;
    for (var i = 0; i < len; ++i) {
        var s = sockets[i];

        // Send msg to the same room member
        if (s.roomname === sock.roomname ) {
            if (s) {
                if (s.username === sock.username ) {
                    s.write(msgOwner);
                } else {
                    s.write(msg);
                }
            }
        }
    }
}

function someoneLeftRoom(sock) {

    var msg = '* user left chat: ' + sock.username + '\n';
    var msgOwner = '* user left chat: ' + sock.username 
                    + ' (** this is you) \n';

    // Broad cast message to this room
    var len = sockets.length;
    for (var i = 0; i < len; i ++) {
        var s = sockets[i];

        // Send to the same room member,
        if (s.roomname === sock.roomname ) {
            if (s) {
                if (s.username === sock.username ) {
                    s.write(msgOwner);
                } else {
                    s.write(msg);
                }
            }
        }
    }

    var members = roomList[sock.roomname].members;
    var index = members.indexOf(sock.username);

    if ( index > -1 ) {
        members.splice(index, 1);
        sys.puts('remove ' + sock.username + ' from room: ' + sock.roomname );
        sock.roomname = '';
    }

    sock.status = ROOM_STATUS;
}

function joinRoom(sock, roomname) {   

    sock.roomname = roomname;
    sys.puts( sock.username + ' joinRoom: ' + roomname);

    roomList[roomname].members.push(sock.username);
    
    // display the room members
    var members = roomList[roomname].members;
    var memberStr = '';
    var membername;

    for( var i = 0; i < members.length; ++i )
    {
        membername = members[i];
        memberStr += '*' + membername;

        if ( membername === sock.username ) {
            memberStr += ' (** this is you)';
        }

        memberStr += '\n';
    }

    // Enter the room, start to chat
    sock.write('entering room: ' + roomname + '\n' + memberStr + '\n' 
        + 'end of list.\n' );
    
    sock.status = CHAT_STATUS;
}

// Create a server instance, and chain the listen function to it
// The function passed to net.createServer() becomes the event handler for the 'connection' event
// The sock object the callback function receives UNIQUE for each connection

// Default rooms in the list
roomList['chat'] = new roomInfo();
roomList['hottub'] = new roomInfo();

net.createServer(function(sock) {
    
    // We have a connection - a socket object is assigned to the connection automatically
    sys.puts('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);    
    sock.write( 'Welcome to Daosheng Mu chat server.\n' 
        + 'Hello ' + sock.remoteAddress + ':' + sock.remotePort + '\n');
    sock.status = LOGIN_STATUS;
    sockets.push(sock);
    sock.write('Login Name? \n');

    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        // Write the data back to the socket, the client will receive it as data from the server
        var msg = ''+data;
        msg = getPureString(msg);

        if (msg === QUIT_CMD) {

            if (sock.status === CHAT_STATUS) {

                sock.write('You can not quit the connection while you are chatting\n' );
            } else {
                sys.puts('exit command received: ' + sock.remoteAddress + ':' + sock.remotePort + '\n');
                sock.write('BYE\n' );
                sock.destroy();
                removeSocket(sock);
                return;
            }
           
        }        
        else { 

            if ( sock.status === LOGIN_STATUS ) {
                var name = msg;

                if ( nameList.indexOf( name ) != -1 ) {
                    // The name has existed
                    sock.write('Sorry, the name: "' + name + '"' + ' name taken. \n'
                        + 'Login Name? \n' );
                }
                else {
                    nameList.push( name );
                    sock.username = name;
                    sys.puts('username: ' + sock.username);
                    sock.status = ROOM_STATUS;

                    sock.write('Welcome ' + sock.username + '!\n' );
                }
            } else if ( sock.status === ROOM_STATUS ) {                
                cmd = msg;

                if (cmd === ROOMS_CMD) {
                    // 
                    showRooms(sock);
                } 
                else if ( cmd.indexOf(JOIN_CMD) > -1 ) {
                    var index = cmd.indexOf(JOIN_CMD) + JOIN_CMD.length;
                    
                    // index + 1 is because there is a space after cmd 
                    var roomname = cmd.substring(index+1, cmd.length);
                    sys.puts(sock.remoteAddress +' enter roomname: ' + roomname);

                    if ( roomList[roomname] ) {
                        joinRoom(sock, roomname);
                        someoneJoinRoom(sock);
                    } else {
                        sock.write('There is no this room... \n');
                    }
                } else if ( cmd.indexOf(CREATE_CMD) > -1 ) {
                    var index = cmd.indexOf(CREATE_CMD) + CREATE_CMD.length;
                    
                    // index + 1 is because there is a space after cmd 
                    var roomname = cmd.substring(index+1, cmd.length);
                    sys.puts(sock.remoteAddress +' create roomname: ' + roomname);

                    if ( roomList[roomname] ) {
                        sock.write('The room name has been used, select another one \n');                        
                    } else {
                        roomList[roomname] = new roomInfo();
                        joinRoom(sock, roomname);
                    }

                } else if ( cmd.indexOf(DELETE_CMD) > -1 ) {
                    var index = cmd.indexOf(DELETE_CMD) + DELETE_CMD.length;
                    
                    // index + 1 is because there is a space after cmd 
                    var roomname = cmd.substring(index+1, cmd.length);
                    sys.puts(sock.remoteAddress +' delete roomname: ' + roomname);

                    if ( !roomList[roomname] ) {
                        sock.write('The room name is not existed \n');                        
                    } else {

                        if ( roomList[roomname].members.length == 0 ) {
                            delete roomList[roomname].members;
                            delete roomList[roomname];
                            sys.puts('remove room: ' + roomList );  
                        } else {
                            sock.write('This room can not be deleted because it is still someone there \n');    
                        }                       
                    }

                } else {
                    sock.write('There is no this command!\n' );
                }  
                
            } else if ( sock.status === CHAT_STATUS ) {                

                if ( msg.indexOf(LEAVE_CMD) > -1 ) {
                    someoneLeftRoom(sock);
                } else {
                    sock.write('You say: ' + msg + '\n' );
                    broadcastToRoom( sock, msg );
                }
            }
        
        }
    });
    
    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        sys.puts('Some connections CLOSED: \n');
        removeSocket(sock);
    });

    sock.on('end', function(data) { // client disconnects
        sys.puts('Disconnected: \n');
        removeSocket(sock);
    });

    //sock is ended by close terminal
    sock.on("error", function(err) {  
        // remove user from chat room
        if ( roomList[sock.roomname] ) {
            var members = roomList[sock.roomname].members;
            var index = members.indexOf(sock.username);

            if ( index > -1 ) {
                members.splice(index, 1);
                sys.puts('remove ' + sock.username + ' from room: ' + sock.roomname );
                sock.roomname = '';
            }   
        }             

        removeSocket(sock);

    });
    
}).listen(PORT);

console.log('Server listening on ' + HOST +':'+ PORT);

