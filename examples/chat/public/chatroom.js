$(function() {

var FADE_TIME = 150; // ms
var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];

var $window = $(window);
var $characterNameInput = $('.usernameInput');
var $messages = $('.messages');
var $inputMessage = $('.inputMessage');
var $newRoomMessage = $('.newRoomMessage');

var $roomSelect = $('#roomSelect');

var $namePage = $('#namePage');
var $roomPage = $('#roomPage');
var $chatPage = $('.chat.page');

var characterName;
var numRooms = 0;
var connected = false;
var socket = io();

// **Message
function addParticipantsMessage (data) {
	var message = '';
	if (data.numUsers === 1) {
	  message += "there's 1 participant";
	} else {
	  message += "there are " + data.numUsers + " participants";
	}
	log(message);
}

// Log a message
function log(message, options) {
	var $el = $('<li>').addClass('log').text(message);
	addMessageElement($el, options);
}

// Add message to message list
function addChatMessage( data, options ) {

	var $usernameDiv = $('<span class="username"/>')
      .text(data.characterName)
      .css('color', getUsernameColor(data.characterName));
	var $messageBodyDiv = $('<span class="messageBody">')
      .text(data.message);

	var $messageDiv = $('<li class="message"/>')
				.data('username', data.characterName)
				.append($usernameDiv, $messageBodyDiv);

	addMessageElement($messageDiv, options);
}

function addMessageElement(el, options) {
	var $el = $(el);

	// Setup default options
    if (!options) {
      options = {};
    }
    if (typeof options.fade === 'undefined') {
      options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
      options.prepend = false;
    }

    // Apply options
    if (options.fade) {
      $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
      $messages.prepend($el);
    } else {
      $messages.append($el);
    }
    $messages[0].scrollTop = $messages[0].scrollHeight;
}

// Sends a chat message
function sendMessage() {
    var message = $inputMessage.val();

    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      addChatMessage({
        characterName: characterName,
        message: message
      });
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', message);
    }
    else {
    	var newRoomMessage = $newRoomMessage.val();

    	if (newRoomMessage) {
    		$('.myModal').hide();
    		socket.emit('join room', newRoomMessage);
    	}
    }
}

// Prevents input from having injected markup
function cleanInput(input) {
	return $('<div/>').text(input).text();
}

// **Room

// Select chat room
function selectChatRoom() {
	// Get room id that character wants to join
	var roomID = roomSelect.options[ roomSelect.selectedIndex ].value;
	// Join the chat room
	socket.emit('join room', roomID);
}

function enterChatRoom(characterList) {

	// Transfer to chat page
	$roomPage.fadeOut();
	$chatPage.show();

	connected = true;

	var message = "Welcome join to the Chatroom";
	log(message, {
		prepend: true
	});

	// Get character list of this room
	log("There have been some friends in this room:", {
		prepend: false
	});

	for ( var i = 0; i < characterList.length; ++i ) {
		log(characterList[i], {
			prepend: false
		});
	}	
}

// **Character
// Gets the color of a username through our hash function
function getUsernameColor(username) {
	// Compute hash code
	var hash = 7;
	for (var i = 0; i < username.length; i++) {
	   hash = username.charCodeAt(i) + (hash << 5) - hash;
	}
	// Calculate color
	var index = Math.abs(hash % COLORS.length);
	return COLORS[index];
}

// Set the character name
function setCharacterName() {
	characterName = $characterNameInput.val();
	
	if (characterName) {
		socket.emit('add character', characterName);
	}
}

// **Key & Button press
// While the button is pressed
$( "#enterRoomBtn" ).click(function() {
	if ( !numRooms ) {
		alert( "Please create a room first." );
		return;
	}

	var roomName = roomSelect.options[ roomSelect.selectedIndex ].value;
	socket.emit('join room', roomName);
});

$( "#createRoomBtn" ).click(function() {
   $('.myModal').show();
});

$( "#closeModal" ).click(function() {
	$('.myModal').hide();
});

// While keyboard has key pressed
$window.keydown(function(event) {

	// if the Enter button is pressed
	if (event.which === 13 ) {
		if (characterName) {   // If characterName is valid
			// Has set the character, we are chatting
			//$chatPage.show();
			sendMessage();
		} 
		else {
			// set character name 
			setCharacterName();
		}
	}
})

// **Socket functions
socket.on('accept username', function(data) {
	// Transfer to chat page
	$namePage.fadeOut();
	$roomPage.show();
});

socket.on('room list', function(data) {
	var rooms = data.roomList;
	var numUsers = data.numUsers;
	var msg;

	// Add room name and number of users to text
	for ( var key in rooms ) {
		msg = key +'('+ numUsers[key] +')';

		$roomSelect
         .append($("<option></option>")
         .attr("value",key)
         .text(msg)); 

        ++numRooms;
	}

	if (numRooms) {
		$roomSelect.selectedIndex = 0;
	}

});

socket.on('room users', function(data) {
	var users = data.roomUsers;

	enterChatRoom(users);
	addParticipantsMessage(data);
});

socket.on('user joined', function(data) {
	log(data.characterName + ' joined');
	addParticipantsMessage(data);
});

socket.on('user left', function(data) {
	log(data.characterName + ' left');
    addParticipantsMessage(data);	
});

socket.on('new message', function(data) {
	addChatMessage(data);
});

socket.on('username used', function(data) {
	characterName = "";
	alert('Character name has been used.');
});

});
