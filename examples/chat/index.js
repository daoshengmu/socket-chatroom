// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('../..')(server);
var port = process.env.PORT || 3200;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

var usernames = {}; // usernames which are currently connected to the chat server
var roomnames = {}; // roomnames which are rooms currently created in this chat server
var roomUsers = {}; // roomUsers, users name of this room
var numRoomUsers = {}; // roomUsers, number of users in this room
var numRooms = 0;   // Number of rooms

io.on('connection', function (socket) {
  var addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.to(socket.roomname).emit('new message', {
      characterName: socket.username,
      message: data
    });
  });

  // when the client emits 'broadcast message', this listens and executes
  socket.on('broadcast message', function (data) {
    // we tell bradcast message to all client to execute 'broadcast message'
    socket.broadcast.emit('broadcast message', {
      characterName: socket.username,
      message: data
    });
  });

  // Character join a room
  socket.on('join room', function (room) {
    socket.roomname = room;

    // If the room isn't exist, we would make a new one.
    if ( !roomnames[room] ) {
      roomnames[room] = room;
      ++numRooms;

      numRoomUsers[room] = 1;
    }
    else {
      ++numRoomUsers[room];
    }

    socket.join(socket.roomname);

    // Add user to room member
    if (!roomUsers[room]) {
      roomUsers[room] = []
    }
    
    roomUsers[room].push(socket.username);  

    // echo the room user list to character
    socket.emit('room users', {
      roomUsers: roomUsers[socket.roomname],
      numUsers: numRoomUsers[socket.roomname]
    });

    // echo to the room that a person has connected
    socket.to(socket.roomname).emit('user joined', {
      characterName: socket.username,
      numUsers: numRoomUsers[socket.roomname]
    });

  });

  // when the client emits 'add user', this listens and executes
  socket.on('add character', function (username) {
    // we store the username in the socket session for this client
    socket.username = username;

    // The user name has been used
    if ( usernames[username] ) {

      socket.emit('username used');
      return;
    }

    // add the client's username to the room
    usernames[username] = username;
    addedUser = true;
    socket.emit('accept username');
    socket.emit('room list', {
      roomList: roomnames,
      numUsers: numRoomUsers
    });

  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.to(socket.roomname).emit('typing', {
      characterName: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.to(socket.roomname).emit('stop typing', {
      characterName: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    // remove the username from global usernames list
    if (addedUser) {
      delete usernames[socket.username];

      // If this user has joined a room, remove him from the room.
      if ( socket.roomname ) {
         --numRoomUsers[socket.roomname];

        // remove user from room user array
        var idx = roomUsers[socket.roomname].indexOf(socket.username);
         if (idx > -1) {
            roomUsers[socket.roomname].splice(idx, 1);
        }

        // If this room is empty, clean this room
        if (numRoomUsers[socket.roomname] === 0 ) {
          delete numRoomUsers[socket.roomname];
          delete roomnames[socket.roomname];
        }

        // echo to the room that this client has left
        socket.to(socket.roomname).emit('user left', {
          characterName: socket.username,
          numUsers: numRoomUsers[socket.roomname]
        });
      }

    }
  });
});
